const path = require('path')
const fs = require('fs')
const filesPath = path.resolve('./files');
const passwordsJSON = path.join(path.resolve('.'), 'passwords.json')

function isEmpty(text, paramName) {
    if (typeof text === "undefined" || text.length === 0) {
        throw new Error(`Please specify '${paramName}' parameter`);
    }
}


function createFile(req, res, next) {
    const filename = req.body.filename;
    const content = req.body.content;
    try {
        isEmpty(content, 'content');
        isEmpty(filename, 'filename');
        fs.writeFile(path.join(filesPath, filename), content, (err) => {
            if (err) {
                throw err;
            }
            res.status(200).send({
                "message": "File created successfully"
            });
        })
    } catch (error) {
        res.status(400).send({
            "message": error.message
        });
    }
}

function getFiles(req, res, next) {
    try {
        fs.readdir(filesPath, (err, files) => {
            if (err) {
                throw err
            }
            res.status(200).send({
                "message": "Success",
                "files": files
            });
        })
    } catch (error) {
        res.status(400).send({
            "message": "Client error"
        });
    }

}

const getFile = (req, res, next) => {
    const filename = req.params.filename;
    const filepath = path.join(filesPath, filename);
    try {
        const stats = fs.statSync(filepath);
        fs.readFile(filepath, 'utf-8', (err, content) => {
            if (err) {
                throw err;
            }
            res.status(200).send({
                "message": "Success",
                "filename": filename,
                "content": content,
                "extension": path.extname(filename).replace('.', ''),
                "uploadedDate": stats.birthtime
            });
        });
    } catch (error) {
        res.status(400).send({
            "message": `No file with '${filename}' filename found`
        });
    }
}

function isFilePresent(filename) {
    if (!fs.readdirSync(filesPath).includes(filename)) {
        throw new Error(`No file with '${filename}' filename found`)
    }
}

const editFile = (req, res, next) => {
    const filename = req.body.filename;
    const content = req.body.content;
    const filepath = path.join(path.resolve('./files'), filename);
    try {
        isEmpty(content, 'content');
        isEmpty(filename, 'filename');
        isFilePresent(filename);
        fs.writeFile(filepath, content, (err) => {
            if (err) {
                throw err;
            }
            res.status(200).send({
                    "message": `File ${filename} was edited successfully`
                }
            )
        });
    } catch (error) {
        res.status(400).send({
            "message": error.message
        })
    }
}

const deleteFile = (req, res, next) => {
    const filename = req.params.filename;
    const filepath = path.join(path.resolve('./files'), filename);
    try {
        isFilePresent(filename);
        fs.rm(filepath, (err) => {
            if (err) {
                throw err;
            }
            res.status(200).send({
                "message": `File ${filename} was deleted successfully`
            });
        });
    } catch (err) {
        res.status(400).send({
            "message": err.message
        })
    }
}

module.exports = {
    createFile,
    getFiles,
    getFile,
    editFile,
    deleteFile
}
